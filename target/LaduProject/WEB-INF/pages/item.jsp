<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="th" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: akimess
  Date: 09/06/15
  Time: 01:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <script src="<c:url value="http://code.jquery.com/jquery-1.11.3.min.js" />"></script>
  <link href="<c:url value="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" />" rel="stylesheet">
  <script src="<c:url value="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js" />"></script>
    <title></title>
</head>
<body>
  <form:form action="/item?action=save" modelAttribute="formsave"  method="POST">
    <div class="form-group">
      <label for="itemid">Item ID</label>
      <input type = "number" class="form-control" name="item" id="itemid"  value="${item.item}" readonly/>
    </div>
    <div class="form-group">
      <label for="itemtitle">Item Title</label>
      <input type = "text" class="form-control" name="name" id="itemtitle"  value="${item.name}" required/>
    </div>
    <div class="form-group">
      <label for="itemdescription">Item Description</label>
      <textarea type = "text" class="form-control" name="description"  id="itemdescription">${item.description}</textarea>
    </div>
    <div class="form-group">
      <label for="storeprice">Store Price</label>
      <input type = "number" class="form-control" name="store_price" id="storeprice"  value="${item.store_price}" required>
    </div>
    <div class="form-group">
      <label for="storeprice">Sale Price</label>
      <input type = "number" class="form-control" name="sale_price" id="saleprice"  value="${item.sale_price}" required>
    </div>
    <input type="submit" value="Save">
  </form:form>
</body>
</html>
