<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>ß
<%--
  Created by IntelliJ IDEA.
  User: akimess
  Date: 09/06/15
  Time: 03:33
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <script src="<c:url value="http://code.jquery.com/jquery-1.11.3.min.js" />"></script>
  <link href="<c:url value="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" />" rel="stylesheet">
  <script src="<c:url value="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js" />"></script>
    <title></title>
  <script>
    function doAjax(id){
      $.getJSON('store?id='+id,function(data){

        if(data.toString().length>0) {
          var table = "<table id='tableItems' class='table'><tr><td>Title</td><td>Description</td><td>Producer Code</td><td></td><td></td></tr>";
          for (var k in data) {
            table += "<tr><td>" + data[k].name + "</td><td>" + data[k].description + "</td><td>"+data[k].producer_code+"</td><td><a href='/store?action=writeoff&id="+data[k].item+"'>Write Off</a></td><td><form action='/storeUp?action=move&pid="+data[k].item+"&wid="+id+"&moveid=<%= request.getParameter("store_id") %>' modelAttribute='formmove' method='post'>Move To <input type='number' name='store_id'> <input type='submit' value='Send'></form></td></tr>";
          }
          table += "</table>";
          //$("#myDiv").append(table);
          var container = document.getElementById('myDiv');
          container.innerHTML = table;
        }else{

          var container = document.getElementById('myDiv');
          container.innerHTML = "NO ITEMS";
        }
      });
    }
  </script>
</head>
<body>
<h1><span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span>Warehouses</h1>
  <ul>
    <c:forEach items="${stores}" var="store">
      <li><a onclick="doAjax(${store.store})">${store.name}</a></li>
    </c:forEach>
  </ul>

<div id = "myDiv">

</div>
</body>
</html>
