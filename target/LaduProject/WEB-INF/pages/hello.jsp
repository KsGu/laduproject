<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
	<script src="<c:url value="http://code.jquery.com/jquery-1.11.3.min.js" />"></script>
	<link href="<c:url value="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" />" rel="stylesheet">
	<script src="<c:url value="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js" />"></script>


</head>
<body>
	<h1><span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span>Functions</h1>
	<div class="btn-group" role="group">
		<button class ="btn btn-default dropdown-toggle" id="dLabel" type="button" data-toggle="dropdown" aria-expanded="false">
			Product Management
			<span class="caret"></span>
		</button>
		<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
			<li><a href="/category?action=searchProduct">Product Search</a></li>
			<li><a href="/category?action=addProduct">Add Product</a></li>
			<li><a href="/category?action=allProducts">All Products</a></li>
		</ul>
	</div>

	<div class="btn-group" role="group">
		<button class ="btn btn-default dropdown-toggle" id="dLabel1" type="button" data-toggle="dropdown" aria-expanded="false">
			Warehouse Operations
			<span class="caret"></span>
		</button>
		<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel1">
			<li><a href="/category?action=regWarehouse">Registration</a></li>
			<li><a href="/category?action=allWarehouses">All Warehouses</a></li>
		</ul>
	</div>

	<div class="btn-group" role="group">
		<button class ="btn btn-default dropdown-toggle" id="dLabel2" type="button" data-toggle="dropdown" aria-expanded="false">
			Price Lists
			<span class="caret"></span>
		</button>
		<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel2">
			<li><a href="/category?action=customerPrice">Customers price list management</a></li>
			<li><a href="/category?action=productPrice">Products price list management</a></li>
		</ul>
	</div>
</body>
</html>