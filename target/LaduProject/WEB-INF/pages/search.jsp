<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: akimess
  Date: 08/06/15
  Time: 00:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <script src="<c:url value="http://code.jquery.com/jquery-1.11.3.min.js" />"></script>
  <link href="<c:url value="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" />" rel="stylesheet">
  <script src="<c:url value="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js" />"></script>
    <title></title>
  <script>

  </script>
</head>
<body>
  <h1><span class="glyphicon glyphicon-tag" aria-hidden="true"></span>Search for product</h1>

  <form action="/search?action=add" modelAttribute="formdata" method="post">
    Title: <input type="text" name="title">
    <br/>
    Description: <input type="text" name="description">
    <br/>
    Manufacturer no.: <input type="text" name="man_code">
    <br/>
    Manufacturer: <input type="text" name="manuf">
    <br/>
    In stock: <input type="number" name="instock">
    <br/>
    Sale price range: <input type="number" name="startPrice"> <input type="number" name="endPrice">
    <br/>
    Warehouse price range: <input type="number" name="startWare"><input type="number" name="endWare">
    <br/>
    Attribute: <input type="text" name="attribute">
    <br/>
    <input type="submit" value="Search">
  </form>
  <table class="table">
    <tr>
      <td>Item Title</td>
      <td>Item Description</td>
      <td>Store Price</td>
      <td>Sale Price</td>
      <td>Producer Code</td>
      <td></td>
    </tr>
    <c:forEach items="${searchResult}" var="sr">
      <tr>
        <td>${sr.name}</td>
        <td>${sr.description}</td>
        <td>${sr.store_price}</td>
        <td>${sr.sale_price}</td>
        <td>${sr.producer_code}</td>
        <td><a href="/item?action=edit&id=${sr.item}">Edit</a></td>
      </tr>
    </c:forEach>
  </table>
</body>
</html>
