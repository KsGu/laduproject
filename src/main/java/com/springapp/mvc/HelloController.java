package com.springapp.mvc;

import com.springapp.mvc.dao.LaduDAO;
import com.springapp.mvc.model.*;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

@Controller
@RequestMapping("/")
public class HelloController {

	@RequestMapping(method = RequestMethod.GET)
	public String printWelcome(ModelMap model) {
		LaduDAO dao = new LaduDAO();
		List<Item> items = dao.getItems();


		model.addAttribute("items", items);

		return "hello";
	}
	@RequestMapping(value = "/category", method = RequestMethod.GET)
	public String categories(@RequestParam(value="action") String par,ModelMap model){
		LaduDAO dao = new LaduDAO();
		if(par.equals("searchProduct")){

			return "search";
		}
		if(par.equals("allProducts") || par.equals("addProduct")) {
			List<ItemType> itemTypesFirst = dao.getFirstTypes();
			String result = "";

			result = drawCatalog(itemTypesFirst, result);
			result = result.replace("<li><a onclick='doAjax(4)'>Metallitoostuse masinad</a></li>", "");
			model.addAttribute("firstTypes", itemTypesFirst);
			model.addAttribute("result", result);
			model.addAttribute("param", par);
			return "category";
		}

		if(par.equals("allWarehouses")){
			List<Store> stores = dao.getAllStores();
			model.addAttribute("stores", stores);
			return "store";
		}
		if(par.equals("regWarehouse")){
			return "registrWarehouse";
		}

		model.addAttribute("error","Please specify a correct action! No such action found!");
		return "error";
	}
	@RequestMapping(value="/item", method=RequestMethod.GET)
	public String items(@RequestParam(value="action") String action, @RequestParam(value="id") String id, ModelMap model){

		if(action.equals("edit")){
			int idInt = 0;
			try{
				idInt = Integer.parseInt(id);
			}catch (Exception ex){
				model.addAttribute("error","Parameter was not a number!");
				return "error";
			}

			LaduDAO dao = new LaduDAO();
			Item item = dao.getItemById(idInt);
			model.addAttribute("item",item);
			return "item";
		}
		return "item";
	}
	@RequestMapping(value = "/item", method = RequestMethod.POST)
	public String postItem(@RequestParam(value = "action") String action, @ModelAttribute("formsave") partItem formdata){
		System.out.println("WENT");
		if(action.equals("save")){
			LaduDAO dao = new LaduDAO();
			System.out.println(formdata.getItem());
			Item updated = dao.getItemById(formdata.getItem());
			updated.setName(formdata.getName());
			updated.setDescription(formdata.getDescription());
			updated.setSale_price(formdata.getSale_price());
			updated.setStore_price(formdata.getStore_price());

			dao.updateItem(updated);
		}

		return "redirect:/category?action=allProducts";
	}

	@RequestMapping(value = "/search",method = RequestMethod.POST)
	public String searchProduct(@RequestParam(value="action") String action, @ModelAttribute("formdata") productSearch formdata,BindingResult result, ModelMap model){
		List<Item> results = new ArrayList<Item>();
		System.out.println("GONE THROUGH AGAIN");
		if(action.equals("add")){
			LaduDAO dao = new LaduDAO();
			 results = dao.searchItems(formdata);

			System.out.println(results.size());
			if(results.size() > 0){
				model.addAttribute("searchResult",results);
			}else{
				model.addAttribute("result","Nothing found.");
			}


		}
		return "search";
	}
	@RequestMapping(value = "/warehouseRegistr", method = RequestMethod.POST)
	public String wareRegistration(@ModelAttribute("warehousereg") Store store,BindingResult result){
		System.out.println("Check on null: " + store.getName());
		if(store.getName().length()>1){
			LaduDAO dao = new LaduDAO();
			dao.insertStore(store);
		}
		return "redirect:/category?action=allWarehouses";
	}
	@RequestMapping(value="/storeUp",method= RequestMethod.POST)
	public String storeOper(@RequestParam(value = "action") String action, @RequestParam(value = "pid") String pid, @RequestParam(value = "wid") String wid, @RequestParam(value="store_id") String moveid, @ModelAttribute("formmove") moveStore toStore){
		int atrp = 0;
		int atrw = 0;
		int atrmov = 0;
		try{
			atrp = Integer.parseInt(pid);
			atrw = Integer.parseInt(wid);
			atrmov = Integer.parseInt(moveid);
		}catch (Exception ex){
			System.out.println("storeOper: " + ex.getMessage());
		}

		if(action.equals("move")){
			LaduDAO dao = new LaduDAO();
			ItemStore store = dao.getItemStoreById(atrp, atrw);
			Item item = dao.getItemById(atrp);
			System.out.println(atrmov);
			if(store != null){
				Store un_store = dao.getStorebyId(atrmov);

					if(un_store != null){
						store.setStore_fk(un_store);
						dao.updateItemStore(store);
					}


			}
		}
		return "redirect:/category?action=allWarehouses";
	}
	@RequestMapping(value="/itemtype",headers="Accept=*/*",method=RequestMethod.GET)
	public @ResponseBody List<Item> productByCategory(@RequestParam(value="type") String type){
		int act = 0;
		List<Item> result = new ArrayList<Item>();

		try{
			act = Integer.parseInt(type);
		}catch (Exception ex){
			System.out.println("productByCategory: " + ex.getMessage());
		}


		LaduDAO dao = new LaduDAO();
		ItemType itemType = dao.getItemTypebyId(act);

		result = getAll(itemType, result);


		return result;
	}

	@RequestMapping(value="/store", headers = "Accept=*/*", method = RequestMethod.GET)
	public @ResponseBody List<Item> productsByStore(@RequestParam(value="id") String id){
		int act = 0;
		List<Item> result = new ArrayList<Item>();

		try{
			act = Integer.parseInt(id);
		}catch(Exception ex){
			System.out.println("productsByStore: " +ex.getMessage());
		}

		LaduDAO dao = new LaduDAO();
		result = dao.getItemsbyStore(act);

		return result;
	}

	public String drawCatalog(List<ItemType> types, String finalResult){
		LaduDAO dao = new LaduDAO();

		for(ItemType type: types){
			System.out.println(type.getType_name());

			List<ItemType> next = dao.getItemTypes(type.getItem_type());
			if(next.size() == 0){
				for(ItemType typeLast: types){
					finalResult = finalResult + "<li><a onclick='doAjax("+typeLast.getItem_type()+")'>"+typeLast.getType_name()+"</a></li>";
				}
				return finalResult;
			}
			finalResult += "<li><a onclick='doAjax("+type.getItem_type()+")'>"+type.getType_name()+"</a><ul>";
			finalResult = drawCatalog(next, finalResult);
			finalResult += "</ul></li>";
		}


		return finalResult;
	}

	public List<Item> getAll(ItemType type, List<Item> all){
		LaduDAO dao = new LaduDAO();
		List<ItemType> itemTypeList = dao.getItemTypes(type.getItem_type());
		List<Item> itemList = dao.itemsByCategory(type);
		if(itemList.size()>0){
			for(Item itm:itemList){
				all.add(itm);
			}
		}
		if(itemTypeList.size()>0){
			for(ItemType typeItem: itemTypeList){
				all = getAll(typeItem, all);
			}
		}

		return all;
	}
}