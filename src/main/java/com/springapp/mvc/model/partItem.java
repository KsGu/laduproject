package com.springapp.mvc.model;

/**
 * Created by akimess on 09/06/15.
 */
public class partItem {
    private int item;
    private String name;
    private String description;
    private double sale_price;
    private double store_price;

    public void setItem(int item){this.item = item;}
    public void setName(String name){this.name = name;}
    public void setDescription(String description){this.description = description;}
    public void setSale_price(double sale_price){this.sale_price = sale_price;}
    public void setStore_price(double store_price){this.store_price = store_price;}

    public int getItem(){return this.item;}
    public String getName(){return this.name;}
    public String getDescription(){return this.description;}
    public double getSale_price(){return this.sale_price;}
    public double getStore_price(){return this.store_price;}
}
