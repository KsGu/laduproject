package com.springapp.mvc.model;

/**
 * Created by akimess on 07/06/15.
 */
public class UnitType {
    private int unit_type;
    private String type_name;
    private String long_name;

    public void setUnit_type(int unit_type){this.unit_type = unit_type;}
    public void setType_name(String type_name){this.type_name = type_name;}
    public void setLong_name(String long_name){this.long_name = long_name;}

    public int getUnit_type(){return this.unit_type;}
    public String getType_name(){return this.type_name;}
    public String getLong_name(){return this.long_name;}
}
