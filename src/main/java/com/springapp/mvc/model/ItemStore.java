package com.springapp.mvc.model;

import org.hibernate.annotations.Entity;

import javax.persistence.CascadeType;
import javax.persistence.ManyToOne;
import java.io.Serializable;

/**
 * Created by akimess on 07/06/15.
 */
@Entity
public class ItemStore implements Serializable {
    private int item_store;

    private Store store_fk;

    private Item item_fk;
    private int item_count;

    public void setItem_store(int item_store){this.item_store = item_store;}
    public void setStore_fk(Store store_fk){this.store_fk = store_fk;}
    public void setItem_fk(Item item_fk){this.item_fk = item_fk;}
    public void setItem_count(int item_count){this.item_count = item_count;}

    public int getItem_store(){return this.item_store;}
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, targetEntity = Store.class)
    public Store getStore_fk(){ return this.store_fk;}
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, targetEntity = Item.class)
    public Item getItem_fk(){return this.item_fk;}
    public int getItem_count(){return this.item_count;}
}
