package com.springapp.mvc.model;



import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by akimess on 06/06/15.
 */

public class Item implements Serializable{
    private int item;
    @JsonIgnore
    private UnitType unit_type;

    private int supplier_enterprise;
    @JsonIgnore
    private ItemType item_type;
    private String name;
    private double store_price;
    private double sale_price;
    private String producer;
    private String description;
    private String producer_code;
    private char single_item;
    private Integer upper_item;
    private String serial_no;
    private Date created;


    public void setItem(int item){ this.item = item;}

    public void setUnit_type(UnitType unit_type){ this.unit_type = unit_type;}

    public void setSupplier_enterprise(int supplier_enterprise){ this.supplier_enterprise = supplier_enterprise;}

    public void setItem_type(ItemType item_type){ this.item_type = item_type;}

    public void setName( String name){this.name = name;}

    public void setStore_price(double store_price){this.store_price = store_price;}

    public void setSale_price(double sale_price){this.sale_price = sale_price;}

    public void setProducer(String producer){this.producer = producer;}

    public void setDescription(String description){this.description = description;}

    public void setProducer_code(String producer_code){this.producer_code = producer_code;}

    public void setSingle_item(char single_item){this.single_item = single_item;}

    public void setUpper_item(Integer upper_item){this.upper_item = upper_item;}

    public void setSerial_no(String serial_no){this.serial_no = serial_no;}

    public void setCreated(Date created){this.created = created;}


    public int getItem(){return this.item;}
    public UnitType getUnit_type(){return this.unit_type;}
    public int getSupplier_enterprise(){return this.supplier_enterprise;}
    public ItemType getItem_type(){return this.item_type;}
    public String getName(){return this.name;}
    public double getStore_price(){return this.store_price;}
    public double getSale_price(){return this.sale_price;}
    public String getProducer(){return this.producer;}
    public String getDescription(){return this.description;}
    public String getProducer_code(){return this.producer_code;}
    public char getSingle_item(){return this.single_item;}
    public Integer getUpper_item(){return this.upper_item;}
    public String getSerial_no(){return this.serial_no;}
    public Date getCreated(){return this.created;}

}
