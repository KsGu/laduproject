package com.springapp.mvc.model;

import java.util.Set;

/**
 * Created by akimess on 07/06/15.
 */
public class ItemType {
    private int item_type;
    private String type_name;
    private int level;
    private int super_type_fk;
    private Set<ItemType> subType;

    public void setItem_type(int item_type){this.item_type = item_type;}
    public void setType_name(String type_name){this.type_name = type_name;}
    public void setLevel(int level){this.level = level;}
    public void setSuper_type_fk(int super_type_fk){this.super_type_fk = super_type_fk;}
    public void setSubType(Set<ItemType> subType){this.subType = subType;}

    public int getItem_type(){return this.item_type;}
    public String getType_name(){return this.type_name;}
    public int getLevel(){return this.level;}
    public int getSuper_type_fk(){return this.super_type_fk;}
    public Set<ItemType> getSubType(){return this.subType;}
}
