package com.springapp.mvc.model;

/**
 * Created by akimess on 08/06/15.
 */
public class productSearch {
    private String title;
    private String description;
    private String man_code;
    private String manuf;
    private int instock;
    private double startPrice;
    private double endPrice;
    private double startWare;
    private double endWare;
    private String attribute;

    public void setTitle(String title){this.title = title;}
    public void setDescription(String description){this.description=description;}
    public void setMan_code(String man_code){this.man_code = man_code;}
    public void setManuf(String manuf){this.manuf = manuf;}
    public void setInstock(int instock){this.instock = instock;}
    public void setStartPrice(double startPrice){this.startPrice = startPrice;}
    public void setEndPrice(double endPrice){this.endPrice = endPrice;}
    public void setStartWare(double startWare){this.startWare = startWare;}
    public void setEndWare(double endWare){this.endWare = endWare;}
    public void setAttribute(String attribute){this.attribute = attribute;}

    public String getTitle(){return this.title;}
    public String getDescription(){return this.description;}
    public String getMan_code(){return this.man_code;}
    public String getManuf(){return this.manuf;}
    public int getInstock(){return this.instock;}
    public double getStartPrice(){return this.startPrice;}
    public double getEndPrice(){return this.endPrice;}
    public double getStartWare(){return this.startWare;}
    public double getEndWare(){return this.endWare;}
    public String getAttribute(){return this.attribute;}

}
