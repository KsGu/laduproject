package com.springapp.mvc.model;

import org.hibernate.annotations.Entity;

import java.io.Serializable;

/**
 * Created by akimess on 07/06/15.
 */
@Entity
public class Store implements Serializable{
    private int store;
    private String name;

    public void setStore(int store){this.store = store;}
    public void setName(String name){this.name = name;}

    public int getStore(){return this.store;}
    public String getName(){return this.name;}
}
