package com.springapp.mvc.dao;

import org.hibernate.SessionFactory;

import org.hibernate.cfg.Configuration;

/**
 * Created by akimess on 06/06/15.
 */
public class HibernateUtil {
    private  static SessionFactory sessionFactory;

    static{
        try{
            //noinspection deprecation
            sessionFactory = new Configuration().configure().buildSessionFactory();
        }catch (Throwable ex){
            System.err.println("Initial SessionFactory creation failed." + ex);
        }
    }

    public static SessionFactory getSessionFactory(){
        return sessionFactory;
    }
}
