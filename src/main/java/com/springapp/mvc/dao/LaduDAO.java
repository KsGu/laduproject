package com.springapp.mvc.dao;

import com.springapp.mvc.model.*;
import org.hibernate.*;


import javax.transaction.Transactional;
import java.util.List;
import java.util.Set;

public class LaduDAO {

    private List<Item> itemList;
    private List<ItemType> itemTypes;
    private ItemType current_itemType;
    private List<ItemType> itemTypeList;

    public List<Item> getItems(){
        itemList = null;
        Session session = null;
        Transaction transaction = null;
        try{
            session = HibernateUtil.getSessionFactory().getCurrentSession();
           transaction =  session.beginTransaction();
            itemList = session.createQuery("from Item as i").list();
            for(Item itm : itemList){
                Hibernate.initialize(itm.getUnit_type());
                Hibernate.initialize(itm.getItem_type());
                Hibernate.initialize(itm.getItem_type().getSubType());
            }
            transaction.commit();
        }catch(Exception ex){
            transaction.rollback();
            System.out.println("itemDAO does not work: " + ex.getMessage());
        }finally {
            //session.close();
        }
        return itemList;
    }

    public List<ItemType> getFirstTypes(){
        itemTypes = null;
        Session session = null;
        Transaction transaction = null;
        try{
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            itemTypes = session.createQuery("from ItemType as it where it.level=1 order by it.item_type").list();
            for(ItemType itype: itemTypes){
                Hibernate.initialize(itype.getSubType());
            }
            transaction.commit();
        }catch(Exception ex){
            transaction.rollback();
            System.out.println("itemTypeDAO does not work: " + ex.getMessage());
        }finally{

        }
        return itemTypes;
    }

    public ItemType getItemTypebyId(int itemtype_id){
        current_itemType = null;
        Session session = null;
        Transaction transaction = null;
        try{
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            Query q = session.createQuery("from ItemType as it where it.item_type =:itemtype_id");
            q.setInteger("itemtype_id",itemtype_id);
            current_itemType = (ItemType) q.uniqueResult();
            transaction.commit();
        }catch(Exception ex){
            transaction.rollback();
            System.out.println("getItemTypebyID: " + ex.getMessage());
        }finally {

        }
        return current_itemType;
    }

    public List<ItemType> getItemTypes(int type_id){

        itemTypeList = null;
        Session session = null;
        Transaction transaction = null;
        try{

            session = HibernateUtil.getSessionFactory().getCurrentSession();

            transaction = session.beginTransaction();
            System.out.println(type_id);
            Query q = session.createQuery("from ItemType as it where super_type_fk=:type_id order by  it.item_type");
            q.setInteger("type_id", type_id);
            itemTypeList = q.list();
            transaction.commit();
        }catch(Exception ex){
            transaction.rollback();
            System.out.println("getItemTypes: " + ex.getMessage());
        }finally {

        }
        return itemTypeList;
    }

    public List<Item> searchItems(productSearch form){
       List<Item> searchedItems = null;
        Session session = null;
        Transaction transaction = null;
        String search_string = "";
        try{
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            //Query q = session.createQuery("from Item as i where to_tsvector() and i.producer_code like '"+form.getMan_code()+"%' and i.description like '%"+form.getDescription()+"%' and i.producer like '%"+form.getManuf()+"%' and (i.sale_price >=:start_sale_price AND i.sale_price <=:end_sale_price) and (i.store_price>=:start_store_price and i.store_price <=:end_store_price)");
            SQLQuery q = session.createSQLQuery("SELECT item.* FROM item, item_attribute  WHERE item_attribute.item_fk = item.item and to_tsvector(item_attribute.value_text) @@ to_tsquery(:attr)  and (item.sale_price >=:start_sale_price AND item.sale_price <=:end_sale_price) and (item.store_price>=:start_store_price and item.store_price <=:end_store_price) and UPPER(item.producer) LIKE UPPER('%"+form.getManuf()+"%') and UPPER(item.description) LIKE UPPER('%"+form.getDescription()+"%') and UPPER(item.producer_code) LIKE UPPER('"+form.getMan_code()+"%') and to_tsvector(item.name) @@ to_tsquery(:search_string)");
            //SQLQuery q = session.createSQLQuery("SELECT item.* FROM item, item_attribute  WHERE item_attribute.item_fk = item.item and item_attribute.value_text=:attr  and (item.sale_price >=:start_sale_price AND item.sale_price <=:end_sale_price) and (item.store_price>=:start_store_price and item.store_price <=:end_store_price) and UPPER(item.producer) LIKE UPPER('%"+form.getManuf()+"%') and UPPER(item.description) LIKE UPPER('%"+form.getDescription()+"%') and UPPER(item.producer_code) LIKE UPPER('"+form.getMan_code()+"%') and item.name=:search_string");
            search_string = form.getTitle();
            q.setString("search_string",search_string);
            q.setParameter("attr",form.getAttribute());
            q.setParameter("start_sale_price", form.getStartPrice());
            q.setParameter("end_sale_price", form.getEndPrice());
            q.setParameter("start_store_price",form.getStartWare());
            q.setParameter("end_store_price",form.getEndWare());
            q.addEntity(Item.class);
            searchedItems = q.list();
            transaction.commit();;
        }catch (Exception ex){
            transaction.rollback();
            System.out.println("searchItems: " + ex.getMessage());
        }finally {

        }
        return searchedItems;
    }

    public List<Item> itemsByCategory(ItemType type){
        List<Item> itemsCategory = null;
        Session session = null;
        Transaction transaction = null;
        try{
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            Query q = session.createQuery("from Item as i where i.item_type=:item_type");
            q.setParameter("item_type", type);
            itemsCategory = q.list();
            transaction.commit();
        }catch (Exception ex) {
            transaction.rollback();
            System.out.println("itemsByCategory: " + ex.getMessage());
        }finally {

        }
        return itemsCategory;
    }

    public Item getItemById(int id){
        Item item = null;
        Session session = null;
        Transaction transaction = null;

        try{
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            Query q = session.createQuery("from Item as i where i.item=:id");
            q.setInteger("id",id);
            item = (Item) q.uniqueResult();
            transaction.commit();
        }catch (Exception ex){
            transaction.rollback();
            System.out.println("getItemById: " +ex.getMessage());
        }finally {

        }
        return item;
    }
    public List<Store> getAllStores(){
        List<Store> stores = null;
        Session session = null;
        Transaction transaction = null;

        try{
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            Query q = session.createQuery("from Store as s");
            stores = q.list();
            transaction.commit();
        }catch (Exception ex){
            transaction.rollback();
            System.out.println("getAllStores: " +ex.getMessage());
        }
        return stores;
    }

    public List<Item> getItemsbyStore(int id){
        List<Item> items = null;
        Session session = null;
        Transaction transaction = null;

        try{
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            Query q = session.createQuery("select itfk from ItemStore ist join ist.item_fk itfk where ist.store_fk.id=:id ");
            q.setInteger("id",id);
            items = q.list();
            transaction.commit();
        }catch (Exception ex){
            transaction.rollback();
            System.out.println("getItemsbyStore: " +ex.getMessage());
        }
        return items;
    }

    public ItemStore getItemStoreById(int pid, int wid){
        ItemStore itemStore = null;
        Session session = null;
        Transaction transaction = null;

        try{
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            Query q = session.createQuery("from ItemStore its where its.item_fk.id=:pid and its.store_fk.id=:wid");
            q.setInteger("pid", pid);
            q.setInteger("wid",wid);
            itemStore = (ItemStore) q.uniqueResult();
            transaction.commit();
        }catch (Exception ex){
            transaction.rollback();
            System.out.println("getItemStockbyId: " +ex.getMessage());
        }
        return itemStore;
    }

    public Store getStorebyId(int id){
        Store store = null;
        Session session = null;
        Transaction transaction = null;

        try{
            session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            Query q = session.createQuery("from Store s where s.id=:id");
            q.setInteger("id",id);
            store = (Store) q.uniqueResult();
            transaction.commit();
        }catch (Exception ex){
            transaction.rollback();
            System.out.println("getStoreById: " +ex.getMessage());
        }
        return store;
    }
    public void updateItem(Item item){
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = null;

        try{
            transaction = session.beginTransaction();
            session.update(item);
            transaction.commit();
        }catch (Exception ex){
            if(transaction !=null) transaction.rollback();
            System.out.print("updateItem: " +ex.getMessage());
        }
    }

    public void updateItemStore(ItemStore store){
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = null;

        try{
            transaction = session.beginTransaction();
            System.out.println("UPDATE");
            session.saveOrUpdate(store);

            session.flush();
            transaction.commit();

        }catch (Exception ex){
            if(transaction !=null) transaction.rollback();
            System.out.println("updateItemStore: " +ex.getMessage());
        }finally {
           // session.close();
        }

    }

    public int insertStore(Store store){

        int store_in = 0;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;

        try{
            transaction = session.beginTransaction();
            session.save(store);
            transaction.commit();
            store_in = store.getStore();
        }catch (Exception ex){
            if(transaction != null) transaction.rollback();
            store_in = -1;
            System.out.println("insertStore: " +ex.getMessage());
        }
        return store_in;
    }

}
