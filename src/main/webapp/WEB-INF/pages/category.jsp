<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: akimess
  Date: 07/06/15
  Time: 21:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <script src="<c:url value="http://code.jquery.com/jquery-1.11.3.min.js" />"></script>
  <link href="<c:url value="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" />" rel="stylesheet">
  <script src="<c:url value="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js" />"></script>
    <title></title>
  <script>
    function doAjax(type_id){
      $.getJSON('itemtype?type='+type_id,function(data){

        if(data.toString().length>0) {
          var table = "<a onclick='ajaxAdd("+type_id+")'>Add Item</a><table id='tableItems' class='table'><tr><td>Title</td><td>Description</td><td>Producer Code</td><td></td></tr>";
          for (var k in data) {
            table += "<tr><td>" + data[k].name + "</td><td>" + data[k].description + "</td><td>"+data[k].producer_code+"</td><td><a href='/item?action=edit&id="+data[k].item+"'>Edit</a></td></tr>";
          }
          table += "</table>";
          //$("#myDiv").append(table);
          var container = document.getElementById('myDiv');
          container.innerHTML = table;
        }else{

          var container = document.getElementById('myDiv');
          container.innerHTML = "<a onclick='ajaxAdd("+type_id+")'>Add Item</a> <br>NO ITEMS";
        }
      });
    }

    function ajaxAdd(type_id){

    }
  </script>
</head>
<body>
<h1><span class="glyphicon glyphicon-tag" aria-hidden="true"></span>Categories</h1>
<br>
  <ul>
    ${result}
  </ul>

  <div id = "myDiv">

  </div>
  <div id="register" style="position:relative; right:-300px; top: -200px;">

  </div>
</body>
</html>
